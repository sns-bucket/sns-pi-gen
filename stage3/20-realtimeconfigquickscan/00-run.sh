#!/bin/bash -e

on_chroot << EOF
if ! [ -d /realtimeconfigquickscan ]; then
  git clone https://github.com/raboof/realtimeconfigquickscan
fi
EOF
