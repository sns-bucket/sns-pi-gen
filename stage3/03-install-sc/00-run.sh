#!/bin/bash -e
install -v -m 755 files/scsynthd "${ROOTFS_DIR}/usr/bin"
install -v -m 644 files/scsynthd.service "${ROOTFS_DIR}/usr/lib/systemd/system"
mkdir -p "${ROOTFS_DIR}/etc/supercollider"
install -v -m 644 files/scsynth.conf "${ROOTFS_DIR}/etc/supercollider"
mkdir -p "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/supercollider/synthdefs"
mkdir -p "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/supercollider/plugins"

on_chroot << EOF
systemctl enable scsynthd
EOF
