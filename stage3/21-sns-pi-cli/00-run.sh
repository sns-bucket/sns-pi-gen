#!/bin/bash -e

on_chroot << EOF
if ! [ -d "/home/${FIRST_USER_NAME}/sns-pi-cli" ]; then
  git clone https://gitlab.com/sns-bucket/sns-pi-cli.git "/home/${FIRST_USER_NAME}/sns-pi-cli"
fi
EOF
