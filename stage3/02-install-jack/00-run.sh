#!/bin/bash -e

on_chroot << EOF
systemctl enable jack
adduser "${FIRST_USER_NAME}" jack
EOF
