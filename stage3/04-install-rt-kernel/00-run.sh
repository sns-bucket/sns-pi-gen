#!/bin/bash -e

on_chroot << EOF
echo "vm.swappiness = 10" >> /etc/sysctl.conf
echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.conf
EOF
