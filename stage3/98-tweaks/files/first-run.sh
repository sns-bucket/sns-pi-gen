#!/bin/sh
mkdir -p ~/.config
if [ ! -e ~/.config/disable-first-boot-msg ]; then
	touch ~/.config/disable-first-boot-msg
	echo "Welcome to sns*os"

	# TODO fails during build because restarting jack fails
	sudo patchbox jack config --rate 44100 --buffer 512 --period 3 --card ALSA
fi
