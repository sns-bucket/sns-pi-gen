#!/bin/bash -e

install -m 644 files/motd ${ROOTFS_DIR}/etc
install -m 644 files/*.service ${ROOTFS_DIR}/usr/lib/systemd/system

# raspi-config is only enabling 'ondemand' governor as of 2018.08.19
on_chroot << EOF
systemctl enable cpu_performance_scaling_governor
systemctl disable raspi-config
EOF

cp -p files/first-run.sh ${ROOTFS_DIR}/etc/profile.d/

