# pi-gen for sns*OS

Forked from [pi-gen](https://github.com/RPi-Distro/pi-gen) with some [patchbox-os-gen](https://github.com/BlokasLabs/patchbox-os-gen/) sugar.

Builds a custom Raspbian image with a realtime kernel and supercollider as a headless service.

**WARNING:**  
This is an experimental project. Do not use it without getting in touch as it may change in unexpected ways. I am willing to change this, so please let me know if you are interested and want to use this image.

**CREDITS:**  
* All credit goes to the developers of [Raspbian](https://www.raspberrypi.org/downloads/raspbian/) and [Patchbox OS](https://blokas.io/patchbox-os/). This repository only contains customizations.
* Motd ascii art by -hrr-

## Getting started

Read the [Known Issues](https://gitlab.com/sns-bucket/sns-pi-gen/-/wikis/Known-Issues)
wiki page first.

The default user is `sns` with password `sns`.

If you want to connect to a wifi network on first boot you can create a `/boot/wpa_supplicant.conf` file on the sdcard containing:

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

# Two character ISO language code. Change it
# if you are not in Germany.
country=DE

network={
 ssid="<YOUR_SSID>"
 psk="<YOUR_KEY>"
}
```

If your are running linux you can create hash using the `wpa_passphrase` command instead of using the cleartext key.

```
wpa_passphrase <YOUR_SSID> <YOUR_KEY>
```

## Build

**WARNING:**  
There is a bug in the original `pi-gen` which results in builds on x64 failing silently. This does not affect the docker build.

I personally build it using docker as I don't have a debian based os running.


```
git clone https://gitlab.com/sns-bucket/sns-pi-gen.git
cd sns-pi-gen

# starting the apt-cacher container is mandatory!
docker-compose up -d

# Add config-local and change it to your needs
cp config-local.example config-local

./build-docker.sh -c config-local
```

Check the [pi-gen/blob/master/README.md](https://github.com/RPi-Distro/pi-gen/blob/master/README.md) for detailed build instructions and informations.

## sns*OS Stage Overview

The build of sns*OS is divided up into several stages for logical clarity
and modularity.

 - **Stage 0**, **Stage 1**, **Stage 2** - raspbian lite
   
   Mostly unchanged stages from `pi-gen`

   **Diff to Raspbian:**
   - Removed `stage2/03-accept-mathematica-eula` as sns*OS is not shipped with Mathematica

 - **Stage 3** - customizations

   This is where the sns*OS-lite image is generated.  
   Adds the Patchbox OS repository and packages including the realtime kernel.
   Also the audio configuration is set up and SuperCollider is installed and configured

## Cheatsheet

```
# List audio hardware
aplay -l

# start jack (replace 'hw:Pro' with your audio device)
/usr/bin/jackd -t 2000 -R -P 95 -d alsa -d hw:Pro -r 48000 -p 512 -n 3 -X seq -s -S

# List jack ports
jack_lsp

# display running kernel
uname -r

# List open ports
netstat -l
```